package edu.ensimag.algo;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import edu.ensimag.algo.partie1.Algo1;
import edu.ensimag.algo.partie23.Algo2;
import edu.ensimag.algo.partie24.Algo3;
import edu.ensimag.algo.partie26.Algo4;

/**
 * Cette classe test les cas êxtremes (dallage tout blanc ou tout noir) pour
 * chaque Algo. Elle test également tous les algo et les compare au résultat de
 * l'algo1 qui donne la bonne réponse à chaque fois. L'algo1 étant testé dans
 * une classe à part.
 *
 * @author tom
 */
public class AlgoTest extends TestCase {

    // Pourcentage du nombre de dalle blanche
    private static int PERCENTAGE_WHITE_SLAB = 80;

    // Taille des dallages
    private static int SIZE = 20;

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AlgoTest.class);
    }

    Map<String, AlgoMere> algo;

    /**
     * Create the test case
     *
     * @param testName
     *            name of the test case
     */
    public AlgoTest(final String testName) {
        super(testName);
    }

    @Override
    public void setUp() {
        algo = new HashMap<>();
        algo.put("algo1", new Algo1());
        algo.put("algo2", new Algo2());
        algo.put("algo3", new Algo3());
        algo.put("algo4", new Algo4());
    }

    /**
     * Test de l'algo 2 : Cherche le plus grand rectangle dans un cas classique
     * généré aléatoirement
     */
    public void testCherchePlusGrandRectangleAlgo2() {
        algo.get("algo1").setDallage(
                Paving.createPaving(SIZE, PERCENTAGE_WHITE_SLAB));
        algo.get("algo2").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo2").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 2 : Cherche le plus grand rectangle dans dallage tout
     * blanc
     */
    public void testCherchePlusGrandRectangleAlgo2DallageBlanc() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 100));
        algo.get("algo2").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo2").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 2 : Cherche le plus grand rectangle dans dallage tout noir
     */
    public void testCherchePlusGrandRectangleAlgo2DallageNoir() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 0));
        algo.get("algo2").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle(),
                algo.get("algo2").cherchePlusGrandRectangle());
    }

    /**
     * Test de l'algo 3 : Cherche le plus grand rectangle dans un cas classique
     * généré aléatoirement
     */
    public void testCherchePlusGrandRectangleAlgo3() {
        algo.get("algo1").setDallage(
                Paving.createPaving(SIZE, PERCENTAGE_WHITE_SLAB));
        algo.get("algo3").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo3").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 3 : Cherche le plus grand rectangle dans dallage tout
     * blanc
     */
    public void testCherchePlusGrandRectangleAlgo3DallageBlanc() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 100));
        algo.get("algo3").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo3").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 3 : Cherche le plus grand rectangle dans dallage tout noir
     */
    public void testCherchePlusGrandRectangleAlgo3DallageNoir() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 0));
        algo.get("algo3").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle(),
                algo.get("algo3").cherchePlusGrandRectangle());
    }

    /**
     * Test de l'algo 4 : Cherche le plus grand rectangle dans un cas classique
     * généré aléatoirement
     */
    public void testCherchePlusGrandRectangleAlgo4() {
        algo.get("algo1").setDallage(
                Paving.createPaving(SIZE, PERCENTAGE_WHITE_SLAB));
        algo.get("algo4").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo4").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 4 : Cherche le plus grand rectangle dans dallage tout
     * blanc
     */
    public void testCherchePlusGrandRectangleAlgo4DallageBlanc() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 100));
        algo.get("algo4").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle().getAire(),
                algo.get("algo4").cherchePlusGrandRectangle().getAire());
    }

    /**
     * Test de l'algo 4 : Cherche le plus grand rectangle dans dallage tout noir
     */
    public void testCherchePlusGrandRectangleAlgo4DallageNoir() {
        algo.get("algo1").setDallage(Paving.createPaving(SIZE, 0));
        algo.get("algo4").setDallage(algo.get("algo1").getDallage());
        assertEquals(algo.get("algo1").cherchePlusGrandRectangle(),
                algo.get("algo4").cherchePlusGrandRectangle());
    }
}
