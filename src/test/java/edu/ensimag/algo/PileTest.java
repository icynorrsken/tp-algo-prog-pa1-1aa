package edu.ensimag.algo;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import edu.ensimag.algo.partie26.Pile;

/**
 * Test de la pile pour la dernière partie du TP
 *
 * Cas testés : - La pile empile les éléments et les restitue dans le bon ordre
 * - La pile émet des exceptions correctement - La pile restitue tous les
 * éléments empilés - La pile ne duplique pas ou ne supprime pas d'élément
 *
 * @author Jerome
 *
 */
public class PileTest extends TestCase {
    public static Test suite() {
        return new TestSuite(Algo1Test.class);
    }

    public PileTest(final String name) {
        super(name);
    }

    /**
     * Test du lancement de l'exception si l'on tente d'accéder à un élément
     * alors que la pile est vide.
     */
    public void depilementPileVideTest() {
        final Pile<Integer> pile = new Pile<>();

        try {
            pile.pop(); // Doit émettre une exception
            fail("La pile n'a pas émit d'exception");
        } catch (final Exception e) {
            assertTrue(true);
        }
    }

    /**
     * Test d'empilement/dépilement aléatoires. A pour but de vérifier que la
     * pile ne provoque pas de disparition ou de duplication d'éléments.
     */
    public void empilementAleatoireTest() {
        final Pile<Integer> pile = new Pile<>();

        // Cette variable contient les valeurs cumulées des éléments
        int valeursCumuleesTousElements = 0;

        // Cette variable contient les valeurs cumulees de tous les éléments
        // dépilés
        int valeursCumuleesElementsDepiles = 0;

        // Ajout de 42 éléments
        for (int a = 1; a <= 42; a++) {
            pile.push(a);

            valeursCumuleesTousElements += a;
        }

        // Des éléments sont empilés/dépilés au hasard
        for (int b = 43; b < 500; b++) {
            if ((Math.random() > 0.5) && !pile.isEmpty()) {
                final int elementDepile = pile.pop();

                valeursCumuleesElementsDepiles += elementDepile;
            } else {
                pile.push(b);
            }

            valeursCumuleesTousElements += b;
        }

        while (!pile.isEmpty()) {
            valeursCumuleesElementsDepiles += pile.pop();
        }

        /*
         * Si la valeur cumulée de tous les éléments (vct) n'est pas égale à la
         * valeur cumulée de tous les éléments qui sont passés par la pile
         * (vcp), on a deux cas de figure : - vcp > vct => La pile a dupliqué
         * des éléments - vct > vcp => La pile a effacé des éléments
         * 
         * Le résultat de ce test est signification que si nombreEmpilementTest
         * a été passé avec succès (sinon, s'il y a duplication d'éléments
         * compensant une dispration, on ne voit passer ni l'une ni l'autre).
         */
        assertEquals(valeursCumuleesTousElements,
                valeursCumuleesElementsDepiles);
    }

    /**
     * Test de la capacité de la pile à empiler les éléments et à les restituer
     * dans l'ordre inverse de l'empilement.
     *
     * Cas d'erreur : - Après empilement d'éléments, la pile est toujours vide -
     * En dépilant, on obtient pas les éléments dans l'ordre inverse de leur
     * empilement - Après dépilement complet, la pile n'est pas vide
     */
    public void empilementSuccessifTest() {
        final Pile<Integer> pile = new Pile<>();

        // Empilement de 42 integers
        for (int a = 1; a <= 42; a++) {
            pile.push(a);
        }

        if (pile.isEmpty()) {
            // La pile ne doit pas être vide
            fail("Pile anormalement vide");
        }

        // Test du dépilement pour voir si les valeurs sont empilées comme il
        // faut
        for (int b = 42; b > 0; b--) {
            if (!pile.pop().equals(b)) {
                fail("Pile ne retournant pas le bon résultat");
            }
        }

        if (!pile.isEmpty()) {
            // La pile doit être vide (tous les éléments popés)
            fail("Pile anormalement pleine");
        }
    }

    /**
     * Test permettant de vérifier que le nombre d'éléments empilés est bien
     * égal au nombre d'éléments dépilables.
     */
    public void nombreEmpilementTest() {
        final Pile<Integer> pile = new Pile<>();

        for (int a = 0; a < 42; a++) {
            pile.push(a);
        }

        int nombreElementsDepiles = 0;
        while (!pile.isEmpty()) {
            pile.pop();
            nombreElementsDepiles++;
        }

        assertEquals(42, nombreElementsDepiles);
    }
}
