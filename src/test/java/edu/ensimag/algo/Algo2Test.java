package edu.ensimag.algo;

import edu.ensimag.algo.partie1.Algo1;
import edu.ensimag.algo.partie23.Algo2;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Protocole de test unitaire de l'algo 2
 * 
 * Cas testés :
 *  - Dallage tout noir
 *  - Dallage tout blanc
 *  - Dallage "classique"
 *  - Comparaison des résultats avec l'algo 1
 * 
 * @author Jerome
 *
 */
public class Algo2Test extends TestCase {
    protected Algo2 algoATester;
    
    public static Test suite() {
        return new TestSuite(Algo2Test.class);
    }
    
    public Algo2Test(String name) {
        super(name);
    }
    
    @Override
    public void setUp() {
        this.algoATester = new Algo2();
    }
    
    /**
     * Test du cas extrême n°1 : dallage tout noir
     */
    public void dallageNoirTest() {
        // Résultat attendu : "null" -> aucun rectangle blanc trouvé
        boolean[][] dallage = new boolean[][] {
                new boolean[] {false, false, false, false, false, false, false, false, false},
                new boolean[] {false, false, false, false, false, false, false, false, false},
                new boolean[] {false, false, false, false, false, false, false, false, false},
                new boolean[] {false, false, false, false, false, false, false, false, false},
                new boolean[] {false, false, false, false, false, false, false, false, false},
        };

        this.algoATester.setDallage(dallage);
        
        assertEquals(null,
                this.algoATester.cherchePlusGrandRectangle());
    }
    
    /**
     * Test du cas extrême n°2 : dallage tout blanc
     */
    public void dallageBlancTest() {
        // Résultat attendu : objet de type AlgoResultat défini depuis (0, 0) vers (9, 5)
        boolean[][] dallage = new boolean[][] {
                new boolean[] {true, true, true, true, true, true, true, true, true},
                new boolean[] {true, true, true, true, true, true, true, true, true},
                new boolean[] {true, true, true, true, true, true, true, true, true},
                new boolean[] {true, true, true, true, true, true, true, true, true},
                new boolean[] {true, true, true, true, true, true, true, true, true},
        };

        this.algoATester.setDallage(dallage);
        
        assertEquals(new AlgoResultat(new Integer[] {0, 0}, new Integer[] {dallage.length, dallage[0].length}),
                this.algoATester.cherchePlusGrandRectangle());
    }
    
    /**
     * Test d'un cas classique :
     * 
     * 0001001
     * 0000101
     * 0010101
     * 1000000
     * 1001001
     */
    public void dallageClassiqueTest() {
        // Résultat attendu : objet de type AlgoResultat défini depuis (0, 0) vers (2, 3)
        boolean[][] dallage = new boolean[][] {
                new boolean[] {true, true, true, false, true, true, false},
                new boolean[] {true, true, true, true, false, true, false},
                new boolean[] {true, true, false, false, true, true, false},
                new boolean[] {false, true, true, true, true, true, true},
                new boolean[] {false, true, true, false, true, true, false},
        };

        this.algoATester.setDallage(dallage);
        
        assertEquals(new AlgoResultat(new Integer[] {0, 0}, new Integer[] {2, 3}),
                this.algoATester.cherchePlusGrandRectangle());
    }
}
