package edu.ensimag.algo;

import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Protocole de test de la super-classe des algos.
 *
 * La méthode AlgoMere.cherchePlusGrandRectangle() retourne null (AlgoMere ne
 * l'implémente pas, mais pour tester la classe il faut qu'elle soit
 * implémentée).
 *
 * Elements testés : - Setter et getter pour le dallage
 *
 * @author Jerome
 *
 */
public class AlgoMereTest extends TestCase {
    public static Test suite() {
        return new TestSuite(Algo1Test.class);
    }

    protected AlgoMere algoATester;

    public AlgoMereTest(final String name) {
        super(name);
    }

    /**
     * Test de la méthode définissant un dallage
     */
    public void setDallageTest() {
        final boolean[][] dallage =
                new boolean[][] {
                        new boolean[] { true, true, false, false, true },
                        new boolean[] { true, false, false, false, true },
                        new boolean[] { false, true, true, false, false }, };

        try {
            algoATester.setDallage(dallage);

            // Voir si le equals d'une ligne est bien exécuté
            assertTrue(Arrays.equals(dallage, algoATester.getDallage()));
        } catch (final Exception e) {
            fail("Problème de définition du dallage");
        }
    }

    @Override
    public void setUp() {
        algoATester = new AlgoMere() {
            // Implémentation de la méthode cherchePlusGrandRectangle() qui ne
            // fait pas partie des tests
            @Override
            public AlgoResultat cherchePlusGrandRectangle() {
                return null;
            }
        };
    }

}
