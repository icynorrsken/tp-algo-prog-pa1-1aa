package edu.ensimag.algo;

import java.io.File;

public class Algo {
    public static void main(final String[] args) {
        final Algo t = new Algo();
        t.read();
        t.create();
    }

    public void create() {
        final boolean[][] paving = Paving.createPaving(5, 50);
    }

    public void read() {
        final boolean[][] paving =
                Paving.parse(new File(Algo.class.getClassLoader()
                        .getResource("dallage.txt").getFile()));
        if (paving == null) {
            System.err.println("Dallage failed");
            System.exit(-1);
        }
        System.out.println("Dallage success");
    }
}
