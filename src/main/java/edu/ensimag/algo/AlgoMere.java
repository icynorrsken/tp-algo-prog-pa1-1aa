package edu.ensimag.algo;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Cette classe permet de définir le dallage utilisé par les algoritmes, qui
 * utilisent tous le même système, inutile donc de réécrire le code.
 *
 * Cette classe est abstraite pour obliger les classes filles à implémenter la
 * méthode de recherche.
 *
 * Cette classe permet de générer facilement un lanceur par algo.
 *
 * @author Jerome, Tom
 *
 */
public abstract class AlgoMere {

    /**
     * Génère un dallage aléatoire avec l'algo de Tom
     * 
     * @param s
     *            Scanner pour obtenir les réponses de l'utilisateur
     * @param o
     *            Pour poser des questions à l'utilisateur
     * @return null en cas d'erreur et un dallage aléatoire en cas de succès
     */
    private static boolean[][] accepteAleatoire(final Scanner s,
            final PrintStream o) {
        o.println("Taille du dammier : ");

        final int taille_carte = s.nextInt();

        if (taille_carte < 1) {
            o.println("Taille du dammier invalide");
        } else {
            o.println("Pourcentage de carrés blancs : ");

            final int pourcentage = s.nextInt();

            if ((pourcentage < 0) || (pourcentage > 100)) {
                o.println("Pourcentage invalide ! Doit être entre 0 et 100");
            } else {
                return Paving.createPaving(taille_carte, pourcentage);
            }
        }

        return null;
    }

    // Codes pour la génération des exécutables
    /**
     * Lit un fichier avec l'algo de lecture de Tom
     * 
     * @param s
     *            Scanner pour obtenir les réponses de l'utilisateur
     * @param o
     *            Pour poser des questions à l'utilisateur
     * @return null si erreur, ou le contenu parsé du fichier
     */
    private static boolean[][] accepterFichier(final Scanner s,
            final PrintStream o) {
        final File fichierSauvegarde = new File("./dal_tomjer.txt");

        o.println("Chemin vers le fichier"
                + (fichierSauvegarde.exists()
                        ? " (laisser vide pour utiliser le fichier sauvegardé)"
                        : "") + " : ");

        final String chemin = s.nextLine().trim();

        File fichier = null;
        if (chemin.equals("") && fichierSauvegarde.exists()) {
            fichier = fichierSauvegarde;
        } else if (!chemin.equals("")) {
            fichier = new File(chemin);
        }

        if (fichier != null) {
            try {
                return Paving.parse(fichier);
            } catch (final Exception e) {
                o.println(e.getMessage());
            }
        }

        return null;
    }

    /**
     * Exécute un algorithme de recherche sur un dallage passé en paramètre.
     * 
     * @param algo
     *            Algorithme
     * @param dallage
     *            Dallage
     * @param o
     *            Flux de sortie pour le texte
     * @return Le résultat de l'exécution (pouvant être null)
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    protected static void executeAlgo(final PrintStream o, final String algo,
            final boolean[][] dallage) throws ClassNotFoundException,
            NoSuchMethodException, SecurityException, InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        // Introspection, pour généraliser la méthode
        final Class classeAlgo = Class.forName(algo);

        final Constructor constructeurAlgo = classeAlgo.getConstructor(null); // Constructeur
                                                                              // par
                                                                              // défaut

        final AlgoMere algor = (AlgoMere) constructeurAlgo.newInstance(null);
        algor.setDallage(dallage);

        final AlgoResultat plusGrandRectangle =
                algor.cherchePlusGrandRectangle();

        o.println("Résultat de la recherche dans le dallage :");
        algor.printDallage(o, plusGrandRectangle);
    }

    /**
     * pour générer un lanceur pour chaque algo
     * 
     * @param args
     */
    public static void main(final String[] args) {
        final PrintStream o = System.out;

        final StackTraceElement[] stackTrace = new Throwable().getStackTrace();

        // Dans le cas où cette méthode est appelée directement depuis la classe
        // mère
        if (stackTrace.length == 1) {
            o.println("La méthode main de la classe AlgoMere ne peut être appelée directement, appelez-là depuis une classe fille.");

            System.exit(-1);
        }

        // Nom de la classe appelant cette méthode
        final String classeAppelante = stackTrace[1].getClassName();

        // Dans le cas où le paramètre associé au .jar est un fichier,
        // l'assistant ne s'exécute pas
        if (args.length == 1) {
            final File dallage = new File(args[0]);
            if (dallage.exists() && dallage.canRead()) {
                o.println("Lanceur auto de l'algo " + classeAppelante);
                o.println("2014 - Tom Lasne & Jerome Barbier");
                o.println("Pour exécuter un assistant d'exécution des algorithmes, veuillez les lancer sans paramètre");
                o.println();

                try {
                    final boolean[][] dallageBool = Paving.parse(dallage);

                    if (dallageBool == null) {
                        o.println("Le dallage n'a pas pu être chargé.\nEssayez d'exécuter l'assistant d'exécution.");

                        System.exit(-1);
                    } else {
                        // Exécution de l'algo sur le dallage
                        try {
                            AlgoMere.executeAlgo(o, classeAppelante,
                                    dallageBool);
                        } catch (final Exception e) {
                            o.println("Une exception a été levée lors de l'exécution de l'algorithme. Détails :\n");
                            e.printStackTrace();

                            System.exit(-1);
                        }
                    }
                } catch (final Exception e) {
                    o.println("Le dallage n'a pas pu être chargé.\nEssayez d'exécuter l'assistant d'exécution.");

                    System.exit(-1);
                }
            } else {
                o.println("Fichier de dallage illisible !");

                System.exit(-1);
            }
        } else {
            o.println("Lanceur de l'algo " + classeAppelante);
            o.println("2014 - Tom Lasne & Jerome Barbier");
            o.println();
            o.println("Tapez \"q\" ou \"quitter\" à tout moment pour arrêter le prompteur");
            o.println();

            final Scanner s = new Scanner(System.in);

            int etat = 0;

            String motLu = "";

            boolean[][] dallage = null;

            while (!motLu.equals("quitter") && !motLu.equals("q")) {
                final int etat_depart = etat;

                if (etat_depart == 0) { // Méthode de génération de la carte
                    if (motLu.equals("aleatoire")) {
                        do {
                            dallage = accepteAleatoire(s, o);
                        } while (dallage == null);

                        etat = 1;
                    } else if (motLu.equals("fichier")) {
                        do {
                            dallage = AlgoMere.accepterFichier(s, o);
                        } while (dallage == null);

                        etat = 1;
                    } else {
                        o.println("Comment souhaitez-vous charger le dallage que vous aller utiliser ? [aleatoire|fichier]");
                    }
                } else if (etat_depart == 1) { // Afficher ou non la carte ?
                    if (motLu.equals("oui")) {
                        AlgoMere.printDallage(o, dallage, null);
                        etat = 2;
                    } else if (motLu.equals("non")) {
                        etat = 2;
                    } else {
                        o.println("Souhaitez-vous voir la carte lue ou générée ? [oui|non]");
                    }
                } else if (etat == 2) { // Exécution algorithme
                    o.println("Exécution de l'algorithme " + classeAppelante
                            + "\n");

                    try {
                        AlgoMere.executeAlgo(o, classeAppelante, dallage);

                        o.println();
                        o.println();

                        etat = 3;
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                } else if (etat == 3) { // Sauvegarde du dallage
                    if (motLu.equals("oui")) {
                        if (AlgoMere.sauvegardeDallage(dallage)) {
                            o.println("La sauvegarde du dallage a été effectuée, vous pouvez l'utiliser avec les autres algorithmes en choisissant de charger le dallage par un fichier, puis en ne marquant rien dans le chemin vers le fichier.");
                        } else {
                            o.println("Une erreur fatale a empêché la création de la sauvegarde.");
                        }
                        etat = 4;
                    } else if (motLu.equals("non")) {
                        etat = 4;
                    } else {
                        o.println("Exécution terminée, souhaitez-vous sauvegarder le dallage ? [oui|non]");
                    }
                } else if (etat == 4) {
                    if (motLu.equals("reex") || motLu.equals("reexecuter")) {
                        etat = 0;
                    } else {
                        o.println("Tout est terminé, souhaitez-vous quitter ou réexécuter ? [quitter|q|reexectuter|reex]");
                    }
                }

                // La console attend un résultat utilisateur
                if (etat_depart == etat) {
                    motLu = s.nextLine().toLowerCase();
                } else {
                    motLu = "";
                }
            }
        }

        System.exit(0);
    }

    /**
     * Version statique du printDallage, pour afficher n'importe quel dallage
     * 
     * @param o
     *            Flux de sortie
     * @param dallage
     *            Dallage à afficher
     * @param montrerRectangle
     *            Rectangle à superposer au dallage, si nécessaire
     */
    public static void printDallage(final PrintStream o,
            final boolean[][] ldallage, final AlgoResultat montrerRectangle) {
        if (ldallage != null) {
            o.println("Représentation du dallage : [   ] pour une case blanche et [ x ] pour une case noire.");
            o.println("Taille de la grille : " + (ldallage.length) + " x "
                    + (ldallage[0].length));

            if (montrerRectangle != null) {
                o.println("Rectangle affiché sur le dallage : "
                        + montrerRectangle + ", cases [ o ].");
            }

            o.println();

            for (int a = 0; a < ldallage.length; a++) {
                for (int b = 0; b < ldallage[a].length; b++) {
                    if (ldallage[a][b]) {
                        String carMilieu = " ";
                        if (montrerRectangle != null) {
                            final Integer[] coordonneesCaseCourantePointHaut =
                                    new Integer[] { a, b };
                            final Integer[] coordonneesCaseCourantePointBas =
                                    new Integer[] { a + 1, b + 1 };

                            if (montrerRectangle.contientCase(new AlgoResultat(
                                    coordonneesCaseCourantePointHaut,
                                    coordonneesCaseCourantePointBas))) {
                                carMilieu = "o";
                            }
                        }
                        o.print("[ " + carMilieu + " ]");
                    } else {
                        o.print("[ x ]");
                    }
                }

                o.print("\n");
            }
        } else {
            o.println("Aucun dallage n'est défini !");
        }
    }

    /**
     * Sauvegarde un dallage dans le fichier dal_tomjer.txt
     * 
     * @param d
     *            Le dallage à sauvegarder
     */
    private static boolean sauvegardeDallage(final boolean[][] d) {
        // Pré-condition : d ne doit pas être null
        final File fichierSauvegarde = new File("dal_tomjer.txt");

        boolean execution = true; // Indicateur de bon déroulement de
                                  // l'opération

        if (!fichierSauvegarde.exists()) {
            try {
                fichierSauvegarde.createNewFile();
            } catch (final IOException e) {
                execution = false;
            }
        }

        if (execution && fichierSauvegarde.exists()) {
            FileOutputStream os = null;

            try {
                os = new FileOutputStream(fichierSauvegarde);

                // Génération du fichier de sauvegarde
                os.write((d.length + " " + d[0].length + "\n").getBytes()); // taille
                                                                            // du
                                                                            // dallage

                // Génération des lignes
                for (int a = 0; a < d.length; a++) {
                    for (int b = 0; b < d[a].length; b++) {
                        if (d[a][b]) {
                            os.write("0".getBytes());
                        } else {
                            os.write("1".getBytes());
                        }
                    }

                    if (a != (d.length - 1)) {
                        os.write("\n".getBytes());
                    }
                }
            } catch (final Exception e) {
                execution = false;
            } finally {
                if (os != null) {
                    try {
                        os.close();
                    } catch (final IOException e) {
                        execution = false;
                    }
                }
            }
        }

        return execution;
    }

    protected boolean[][] dallage;

    /**
     * Création d'une nouvelle instance sans dallage
     */
    public AlgoMere() {
        dallage = null;
    }

    /**
     * Création d'une nouvelle instance avec un dallage par défaut
     * 
     * @param dallage
     *            Le dallage par défaut
     */
    public AlgoMere(final boolean[][] dallage) {
        setDallage(dallage);
    }

    /**
     * Méthode exécutant l'algorithme de recherche, doit être implémenté en
     * fonction des différentes consignes.
     * 
     * @return Un résultat de recherche si un rectangle a été trouvé ou null
     *         sinon.
     */
    abstract public AlgoResultat cherchePlusGrandRectangle();

    protected int[][] determinerHauteur(final boolean[][] dallage) {
        final int[][] hauteurs =
                new int[getDallage().length][getDallage()[0].length];

        // Détermination des hauteurs de rectangles possibles.
        for (int i = 0; i < getDallage().length; i++) {
            for (int j = 0; j < getDallage()[i].length; j++) {
                if (i == 0) {
                    hauteurs[i][j] = 1;
                } else {
                    hauteurs[i][j] = hauteurs[i - 1][j] + 1;
                }

                if (!dallage[i][j]) {
                    hauteurs[i][j] = 0;
                }
            }
        }
        return hauteurs;
    }

    /**
     * FIXME: Peut-être mettre ça dans AlgoResultat ?
     * 
     * @param rectangle
     * @return
     */
    protected int getAire(final Rectangle rectangle) {
        return (int) (rectangle.getWidth() * rectangle.getHeight());
    }

    /**
     * Retourne le dallage défini pour l'instance
     * 
     * @return Le dallage, ou null si aucun dallage n'est défini pour l'instance
     */
    public boolean[][] getDallage() {
        if (dallage != null) {
            return dallage;
        }

        return null;
    }

    /**
     * Compare tous les rectangles de la liste et renvoie le plus grand
     * 
     * @param rectangles
     * @return Le rectangle ayant la plus grande aire
     */
    protected Rectangle plusGrandeAire(final List<Rectangle> rectangles) {
        // Pré-condition : La liste ne doit pas être vide.
        Rectangle plusGrandRectangle = new Rectangle(rectangles.get(0));
        rectangles.remove(0);
        for (final Rectangle rectangle : rectangles) {
            if (rectangle != null) {
                if ((getAire(rectangle) > getAire(plusGrandRectangle))) {
                    plusGrandRectangle = new Rectangle(rectangle);
                }
            }
        }
        return plusGrandRectangle;
    }

    /**
     * Affichage du dallage à l'écran
     * 
     * @param o
     *            Flux de sortie
     * @param montrerRectangle
     *            Rectangle à afficher sur le dallage (null pour ne rien
     *            afficher)
     */
    public void printDallage(final PrintStream o,
            final AlgoResultat montrerRectangle) {
        AlgoMere.printDallage(o, getDallage(), montrerRectangle);
    }

    /**
     * Définition d'un nouveau dallage pour la recherche. Ce dallage doit avoir
     * une dimension correcte du type (x, y) telle que ni x ni y ne soit égal à
     * 0.
     *
     * @param dallage
     *            Le dallage à affecter à l'instance
     * @throws Une
     *             exception si le dallage donné n'est pas correct
     */
    public void setDallage(final boolean[][] dallage) {
        if ((dallage.length == 0) || (dallage[0].length == 0)) {
            throw new RuntimeException("Dimensions du dallage incorrecte");
        }

        this.dallage = dallage;
    }
}