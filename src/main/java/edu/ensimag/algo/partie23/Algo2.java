package edu.ensimag.algo.partie23;

import edu.ensimag.algo.AlgoMere;
import edu.ensimag.algo.AlgoResultat;

/**
 * Algo n°2 selon la technique de l'élimination de lignes lorsque l'on tombe sur
 * un carré noir.
 *
 * @author Jerome
 *
 */
public class Algo2 extends AlgoMere {
    public static void main(final String[] args) {
        AlgoMere.main(args);
    }

    @Override
    public AlgoResultat cherchePlusGrandRectangle() {
        if (dallage != null) {
            AlgoResultat plusGrandRectangle = null;

            for (int a = 0; a < dallage.length; a++) {
                for (int b = 0; b < dallage[a].length; b++) {
                    int xmax = dallage[a].length;

                    final boolean finDeLiteration = false; // Quand le rectangle
                                                           // que l'on décrit a
                                                           // fini d'être décrit

                    // Recherche des positions
                    for (int c = a; (c < dallage.length) && !finDeLiteration; c++) {
                        for (int d = b; d < xmax; d++) {
                            AlgoResultat rectangleATester = null;

                            if (!dallage[c][d]) {
                                xmax = d;

                                if (d != b) {
                                    rectangleATester =
                                            new AlgoResultat(new Integer[] { a,
                                                    b }, new Integer[] { c + 1,
                                                    d });
                                }
                            }

                            if (d == (xmax - 1)) {
                                rectangleATester =
                                        new AlgoResultat(
                                                new Integer[] { a, b },
                                                new Integer[] { c + 1, d + 1 });
                            }

                            // Un rectangle a été généré (il faut regarder si on
                            // doit le considérer comme
                            // nouveau plus grand rectangle)
                            if (rectangleATester != null) {
                                if ((plusGrandRectangle == null)
                                        || (rectangleATester.getAire() > plusGrandRectangle
                                                .getAire())) {
                                    plusGrandRectangle = rectangleATester;
                                }
                            }

                        }
                    }
                }
            }

            return plusGrandRectangle;
        }
        return null;
    }
}
