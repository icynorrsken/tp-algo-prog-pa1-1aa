package edu.ensimag.algo;

import java.awt.Rectangle;
import java.util.Arrays;

/**
 * Cet objet représente un résultat de recherche.
 *
 * Il stocke les coordonnées des cases aux extréités du rectangle et permet de
 * calculer une aire en fonction de ces dernieres.
 * 
 * @author Jerome, Tom
 *
 */
public class AlgoResultat {
    protected Integer[] premiereCase;
    protected Integer[] secondeCase;

    /**
     * Création d'une instance de résultat
     * 
     * @param rectangle
     *            Un objet de type AlgoResultat
     */
    public AlgoResultat(final AlgoResultat rectangle) {
        this(rectangle.getPremiereCase(), rectangle.getSecondeCase());
    }

    /**
     * Création d'une instance de résultat
     * 
     * @param premiereCase
     *            Un tableau à deux cases représentant (x1, y1)
     * @param secondeCase
     *            Un tableau à deux cases représentant (x2, y2) tel que x1 < x2
     *            et y1 < y2
     * @throws Une
     *             exception si les coordonnées ne sont pas valides
     */
    public AlgoResultat(final Integer[] premiereCase,
            final Integer[] secondeCase) {
        if ((premiereCase == null) || (secondeCase == null)
                || (premiereCase.length != 2) || (secondeCase.length != 2)
                || (premiereCase[0] > secondeCase[0])
                || (premiereCase[1] > secondeCase[1])) {
            throw new RuntimeException("Formats de coordonnées invalides : ("
                    + premiereCase[0] + ", " + premiereCase[1] + ")" + ", ("
                    + secondeCase[0] + ", " + secondeCase[1] + ")");
        }

        this.premiereCase = premiereCase;
        this.secondeCase = secondeCase;
    }

    /**
     * Création d'une instance de résultat à partir d'un objet rectangle
     * 
     * @param rectangle
     *            Un objet de type Rectangle
     */
    public AlgoResultat(final Rectangle rectange) {
        final Integer[] premiereCase = new Integer[2];
        final Integer[] secondeCase = new Integer[2];
        premiereCase[0] = rectange.x;
        premiereCase[1] = rectange.y;
        secondeCase[0] = rectange.x + rectange.height;
        secondeCase[1] = rectange.y + rectange.width;
        if ((premiereCase == null) || (secondeCase == null)
                || (premiereCase.length != 2) || (secondeCase.length != 2)
                || (premiereCase[0] > secondeCase[0])
                || (premiereCase[1] > secondeCase[1])) {
            throw new RuntimeException("Formats de coordonnées invalides : ("
                    + premiereCase[0] + ", " + premiereCase[1] + ")" + ", ("
                    + secondeCase[0] + ", " + secondeCase[1] + ")");
        }

        this.premiereCase = premiereCase;
        this.secondeCase = secondeCase;
    }

    /**
     * Retourne true si le rectangle défini dans le résultat contient la case
     * donnée.
     * 
     * @param lacase
     *            La case à tester
     * @return true si la case est contenue dans le rectangle
     */
    public boolean contientCase(final AlgoResultat lacase) {
        // Pas de pré-requis sur les paramètres (formule mathématique)
        return (premiereCase[0] <= lacase.premiereCase[0])
                && (premiereCase[1] <= lacase.premiereCase[1])
                && (secondeCase[0] >= lacase.secondeCase[0])
                && (secondeCase[1] >= lacase.secondeCase[1]);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlgoResultat other = (AlgoResultat) obj;
        if (!Arrays.equals(premiereCase, other.premiereCase)) {
            return false;
        }
        if (!Arrays.equals(secondeCase, other.secondeCase)) {
            return false;
        }
        return true;
    }

    public int getAire() {
        // Pré-requis pour les éléments : (x, y) définis tels que x >= 0 et y >=
        // 0
        return (premiereCase[0] - secondeCase[0])
                * (premiereCase[1] - secondeCase[1]);
    }

    public Integer[] getPremiereCase() {
        return premiereCase;
    }

    public Integer[] getSecondeCase() {
        return secondeCase;
    }

    /**
     * Retourne true si le résultat représenté par cette instance est un point
     * (même coordonnée de début et de fin)
     * 
     * @return True si l'élément est un point
     */
    public boolean isPoint() {
        return Arrays.equals(premiereCase, secondeCase);
    }

    public void setPremiereCase(final Integer[] premiereCase) {
        if (premiereCase.length != 2) {
            throw new RuntimeException("Taille de tableau incorrecte");
        }
        this.premiereCase = premiereCase;
    }

    public void setSecondeCase(final Integer[] secondeCase) {
        if (secondeCase.length != 2) {
            throw new RuntimeException("Taille de tableau incorrecte");
        }
        this.secondeCase = secondeCase;
    }

    @Override
    public String toString() {
        return "(" + premiereCase[0] + ", " + premiereCase[1] + "), " + "("
                + secondeCase[0] + ", " + secondeCase[1] + ")" + ", aire : "
                + getAire();
    }
}
