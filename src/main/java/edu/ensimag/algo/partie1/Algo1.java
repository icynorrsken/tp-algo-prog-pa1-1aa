package edu.ensimag.algo.partie1;

import edu.ensimag.algo.AlgoMere;
import edu.ensimag.algo.AlgoResultat;

public class Algo1 extends AlgoMere { // AlgoMere gère le dallage

    public static void main(final String[] args) {
        AlgoMere.main(args);
    }

    @Override
    public AlgoResultat cherchePlusGrandRectangle() {
        if (dallage != null) {
            AlgoResultat resultatRecherche = null;

            final boolean[][] dallageAvecBordure =
                    new boolean[dallage.length + 1][dallage[0].length + 1];

            // Copie du dallage
            for (int a = 0; a < dallage.length; a++) {
                for (int b = 0; b < dallage[a].length; b++) {
                    dallageAvecBordure[a][b] = dallage[a][b];
                }
            }

            // Les bons éléments sont placés à false pour éviter les
            // surprises dans les algos
            for (int a = 0; a < dallageAvecBordure.length; a++) { // Dernière
                                                                  // colonne
                dallageAvecBordure[a][dallageAvecBordure[0].length - 1] = false;
            }

            for (int a = 0; a < dallageAvecBordure[0].length; a++) { // Dernière
                                                                     // ligne
                dallageAvecBordure[dallageAvecBordure.length - 1][a] = false;
            }

            // Ces deux boucles énumèrent des tailles de rectangles à tester
            for (int a = 1; a < dallageAvecBordure.length; a++) {
                for (int b = 1; b < dallageAvecBordure[a].length; b++) {

                    // Cette boucle prend chaque point et essaie de voir si pour
                    // chacun
                    // des points il est possible de placer un rectangle blanc
                    // sans qu'il
                    // ne soit traversé par un carré noir
                    for (int c = 0; c < dallageAvecBordure.length; c++) {
                        for (int d = 0; d < dallageAvecBordure[c].length; d++) {
                            if (((a + c) >= dallageAvecBordure.length)
                                    || ((d + b) >= dallageAvecBordure[a + c].length)) {
                                break; // Passage à la ligne du dessous si on
                                       // sort de la grille
                            }

                            if (dallageAvecBordure[c][d]) {
                                // Vérification de la non-présence d'un carré
                                // noir dans la zone
                                boolean neContientPas = true;
                                for (int e = c; (e < (c + a)) && neContientPas; e++) {
                                    for (int f = d; f < (d + b); f++) {
                                        if (!dallageAvecBordure[e][f]) {
                                            neContientPas = false;
                                            break;
                                        }
                                    }
                                }

                                if (neContientPas) {
                                    final AlgoResultat rectangleBlancValide =
                                            new AlgoResultat(new Integer[] { c,
                                                    d }, new Integer[] { c + a,
                                                    d + b });

                                    if ((rectangleBlancValide.getAire() != 0)
                                            && ((resultatRecherche == null) || (resultatRecherche
                                                    .getAire() < rectangleBlancValide
                                                    .getAire()))) {
                                        resultatRecherche =
                                                rectangleBlancValide;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return resultatRecherche;
        }

        return null;
    }
}
