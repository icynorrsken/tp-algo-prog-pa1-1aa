package edu.ensimag.algo.exception;

public class FileFormatException extends Exception {
    private static final long serialVersionUID = 1L;

    public FileFormatException() {
        super();
    }

    public FileFormatException(final String message) {
        super(message);
    }
}
