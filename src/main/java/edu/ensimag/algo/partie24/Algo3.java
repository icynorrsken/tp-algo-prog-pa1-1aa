package edu.ensimag.algo.partie24;

import java.awt.Rectangle;

import edu.ensimag.algo.AlgoMere;
import edu.ensimag.algo.AlgoResultat;

/**
 * @author tom
 *
 */
public class Algo3 extends AlgoMere {
    public static void main(final String[] args) {
        AlgoMere.main(args);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.ensimag.algo.AlgoMere#cherchePlusGrandRectangle()
     */
    @Override
    public AlgoResultat cherchePlusGrandRectangle() {
        Rectangle plusGrandRectangle = null;
        if (dallage != null) {
            final int[][] hauteurs = determinerHauteur(getDallage());

            // Parcours des lignes
            for (int i = 0; i < hauteurs.length; i++) {

                // Hauteur minimale du rectangle en cours de création
                int minH = 0;

                // Toutes les colonnes de départ
                for (int d = 0; d < hauteurs[i].length; d++) {

                    // Toutes les colonnes de fin
                    for (int f = d; f < hauteurs[i].length; f++) {

                        // Hauteur de la case en cours
                        final int h = hauteurs[i][f];

                        if (h == 0) {
                            break;
                        }

                        // Affectation de la plus petite hauteur
                        if (f == d) {
                            minH = h;
                        } else if (minH > h) {
                            minH = h;
                        }

                        // Potentiel rectangle le plus grand
                        final Rectangle rectangle =
                                new Rectangle((i - minH) + 1, d, (f + 1) - d,
                                        minH);

                        if (plusGrandRectangle == null) {
                            plusGrandRectangle = rectangle;
                        } else {
                            if (getAire(plusGrandRectangle) < getAire(rectangle)) {
                                plusGrandRectangle = rectangle;
                            }
                        }
                    }
                }
            }
        }

        return plusGrandRectangle == null ? null : new AlgoResultat(
                plusGrandRectangle);
    }
}
