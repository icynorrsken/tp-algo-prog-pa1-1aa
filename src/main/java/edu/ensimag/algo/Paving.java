package edu.ensimag.algo;

import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.ensimag.algo.exception.FileFormatException;

/**
 * Génération des dallages. Fixe par lecture de fichier et aléatoire.
 *
 * @author Tom
 *
 */
public class Paving {
    /**
     * Crée un dallage de taille size*size avec un certain pourcentage de case
     * blanche
     *
     * @param size
     *            la largeur et hauteur du dallage
     * @param percentageWhiteSlab
     *            Le pourcentage de case blanche dans le dallage (ie. 98 pour
     *            98%)
     * @return le dallage
     */
    public static boolean[][] createPaving(final int size,
            final int percentageWhiteSlab) {
        final boolean[][] paving = new boolean[size][size];

        // Nombre moyen de case blanche
        final int whiteCaseNum = (size * size * percentageWhiteSlab) / 100;

        // List des index du dallage
        final List<Point> index = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                index.add(new Point(i, j));
            }
        }

        // Mélange uniforme des index
        Collections.shuffle(index);

        // On prend les (size * size * percentageWhiteSlab) / 100 premières
        // case.
        // De cette manière, il est impossible de choisir deux fois la même
        // case.
        for (int whiteSlab = 0; whiteSlab < whiteCaseNum; whiteSlab++) {
            final Point whiteCasePos = index.get(whiteSlab);
            paving[whiteCasePos.x][whiteCasePos.y] = true;
        }
        return paving;
    }

    /**
     * Vérifie si le code ASCII représente bien un chiffre
     *
     * @param num
     *            le code ASCII à vérifier
     * @return True si le code ASCII passé en paramètre est un chiffre
     */
    private static boolean isNumber(final int num) {
        if ((num > 57) || (num < 48)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Vérifie si le code ASCII représente bien un chiffre compris entre le min
     * et le max passé en paramètre
     *
     * @param num
     * @param min
     * @param max
     * @return True si le code ASCII passé en paramètre est un chiffre et est
     *         compris entre le min et le max
     */
    private static boolean isNumberBetween(final int num, final int min,
            final int max) {
        if ((num > 57) || (num < 48)) {
            return false;
        }
        final int value = Character.getNumericValue(num);
        if ((value < min) || (value > max)) {
            return false;
        }

        return true;
    }

    /**
     * Parser un fichier afin d'obtenir un dallage de boolean
     *
     * @param file
     *            Fichier à parser
     * @return un dallage de boolean
     */
    public static boolean[][] parse(final File file) {
        try {
            if (!file.exists()) {
                throw new FileNotFoundException("Le fichier : "
                        + file.getName() + " n'existe pas.");
            }
            if (!(file.canRead() && file.isFile())) {
                throw new FileSystemException(
                        "Le fichier ne peut être lu ou n'est pas un fichier normal.");
            }
            FileInputStream fis;
            fis = new FileInputStream(file);
            boolean[][] paving = null;
            int line = 1;

            // Parcour de toutes les lignes du fichier
            while (fis.available() > 0) {
                if (line == 1) {
                    paving = Paving.parseFirstLine(fis);
                    line++;
                } else {
                    Paving.parseOtherLine(fis, paving, paving.length,
                            paving[0].length);
                }
            }
            fis.close();
            return paving;
        } catch (final NumberFormatException nfe) {
            new NumberFormatException(
                    "Le fichier ne peut contenir ce caractère : "
                            + nfe.getCause());
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Permet de parser la première ligne du fichier et de récupérer la taille
     * du dallage
     *
     * @param fis
     * @return un dallage de boolean avec une taille définie
     * @throws FileFormatException
     * @throws IOException
     */
    private static boolean[][] parseFirstLine(final FileInputStream fis)
            throws FileFormatException, IOException {
        int h = 0, w = 0;
        int current;
        boolean next = false;
        do {
            current = fis.read();
            if (current == ' ') {
                continue;
            }
            while ((current != ' ') && (current != '\n')) {
                if (!Paving.isNumber(current)) {
                    throw new FileFormatException(
                            "La définition du dallage ne peut contenir que des chiffres.");
                }
                current = Character.getNumericValue(current);
                if (!next) {
                    if (h == 0) {
                        h = current;
                    } else {
                        h = (h * 10) + current;
                    }
                } else if (w == 0) {
                    w = current;
                } else {
                    w = (w * 10) + current;
                }
                current = fis.read();
            }
            next = true;
        } while (current != '\n');
        if ((h <= 0) || (w <= 0)) {
            throw new FileFormatException(
                    "La définition de la taille du tableau ne peut pas être inférieur ou égale à zéro !");
        }
        return new boolean[h][w];
    }

    /**
     * Parse toutes les lignes du fichier (sauf le première) afin de créer un
     * dallage de boolean
     *
     * @param fis
     * @param paving
     * @param h
     *            Hauteur du dallage
     * @param w
     *            Largeur du dallage
     * @return Retourne le dallage créer via le fichier
     * @throws IOException
     * @throws FileFormatException
     */
    private static boolean[][] parseOtherLine(final FileInputStream fis,
            final boolean[][] paving, final int h, final int w)
                    throws IOException, FileFormatException {
        int line = 2;
        int current = 0;
        int pos = 0;
        while (fis.available() > 0) {
            current = fis.read();
            if (current == '\n') {
                if (pos < w) {
                    throw new FileFormatException(
                            "La définition du tableau ne comporte pas suffisament de données.");
                }
                line++;
                pos = 0;
                continue;
            }
            if (!Paving.isNumberBetween(current, 0, 1)) {
                throw new FileFormatException(
                        "La définition du dallage ne peut contenir que des 0 ou des 1.");
            }
            current = Character.getNumericValue(current);
            if ((pos >= w) || ((line - 2) > h)) {
                throw new FileFormatException(
                        "Le fichier comporte plus d'élément que définie.");
            }
            paving[line - 2][pos] = current == 1 ? false : true;
            pos++;
        }
        if ((line - 1) != h) {
            throw new FileFormatException(
                    "La définition du tableau ne comporte pas suffisament de données.");
        }
        return paving;
    }
}
