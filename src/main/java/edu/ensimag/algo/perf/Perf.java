/**
 * Perf
 *
 * Version information
 *
 * 19 Oct 2014
 *
 * Copyright notice
 */

package edu.ensimag.algo.perf;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import edu.ensimag.algo.AlgoMere;
import edu.ensimag.algo.Paving;
import edu.ensimag.algo.partie1.Algo1;
import edu.ensimag.algo.partie23.Algo2;
import edu.ensimag.algo.partie24.Algo3;
import edu.ensimag.algo.partie26.Algo4;

/**
 * @author tom
 *
 */
public class Perf {
    public static void main(final String[] args) throws FileNotFoundException {
        final Map<String, AlgoMere> algos = new HashMap<>();
        algos.put("algo1", new Algo1());
        algos.put("algo2", new Algo2());
        algos.put("algo3", new Algo3());
        algos.put("algo4", new Algo4());

        long b;
        long e;
        final long bt = System.currentTimeMillis();
        long et;

        for (int size = 20; size <= 500; size *= 2) {
            final PrintWriter csv = new PrintWriter("perf/" + size + ".csv");

            csv.write("Pourcentage,Algo1,Algo2,Algo3,Algo4\n");

            for (int percentage = 0; percentage <= 100; percentage += 10) {
                final boolean[][] dallage =
                        Paving.createPaving(size, percentage);
                csv.write(percentage + ",");
                for (final Entry<String, AlgoMere> entry : algos.entrySet()) {
                    entry.getValue().setDallage(dallage);
                    b = System.currentTimeMillis();
                    entry.getValue().cherchePlusGrandRectangle();
                    e = System.currentTimeMillis();
                    final long t = e - b;

                    if (entry.getKey().equals("algo4")) {
                        csv.write(t + "");
                    } else {
                        csv.write(t + ",");
                    }

                }
                csv.write("\n");
            }

            csv.close();
        }

        et = System.currentTimeMillis();

        final double t = ((et - bt) / 1000) / 60;
        System.out.println("Durée totale des tests : " + (t) + " minutes");
    }
}
