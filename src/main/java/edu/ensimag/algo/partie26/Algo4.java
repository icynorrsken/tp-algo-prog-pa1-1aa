package edu.ensimag.algo.partie26;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import edu.ensimag.algo.AlgoMere;
import edu.ensimag.algo.AlgoResultat;

/**
 * @author tom
 *
 */
public class Algo4 extends AlgoMere {
    public static void main(final String[] args) {
        AlgoMere.main(args);
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.ensimag.algo.AlgoMere#cherchePlusGrandRectangle()
     */
    @Override
    public AlgoResultat cherchePlusGrandRectangle() {
        Rectangle plusGrandRectangle = null;
        if (dallage != null) {
            final int[][] hauteurs = determinerHauteur(getDallage());

            // Pile contenant les potentiels rectangles maximaux
            Pile<Rectangle> pile;

            for (int i = 0; i < hauteurs.length; i++) {
                pile = new Pile<>();

                for (int j = 0; j < hauteurs[i].length; j++) {

                    // Hauteur de la case en cours
                    final int h = hauteurs[i][j];

                    if (h == 0) {
                        while (!pile.isEmpty()) {
                            final Rectangle rectangle = pile.pop();
                            rectangle
                            .setSize(j - rectangle.y, rectangle.height);
                            if (plusGrandRectangle == null) {
                                plusGrandRectangle = new Rectangle(rectangle);
                            } else if (getAire(plusGrandRectangle) < getAire(rectangle)) {
                                plusGrandRectangle = new Rectangle(rectangle);
                            }
                        }
                        continue;
                    }

                    if (j != 0) {
                        if (hauteurs[i][j - 1] == h) {
                            continue;
                        }

                        // Push d'un nouveau rectangle
                        if (hauteurs[i][j - 1] < h) {
                            pile.push(new Rectangle((i - h) + 1, j, 0, h));
                        }
                        // On ferme les rectangles dont la hauteur est
                        // supérieure à
                        // la hauteur de le colonne actuelle
                        else {

                            // Position du coin supérieur du rectangle ayant une
                            // hauteur supérieure à la hauteur actuelle
                            // (potentiellement le dernier de ces rectangles)
                            // Cette position permettra d'initialiser un nouveau
                            // rectangle avec la hauteur actuelle si necessaire
                            Point p = null;
                            final Pile<Rectangle> tmp = new Pile<>();

                            // Parcours des rectangles qui ont une hauteur
                            // supérieur
                            // à la hauteur actuelle
                            while (!pile.isEmpty()) {
                                final Rectangle rectangle = pile.pop();

                                // Si la hauteur du rectangle est plus grande
                                // que celle du rectangle actuelle, on calcul
                                // son aire pour vérifier si c'est le plus grand
                                // rectangle
                                if (rectangle.height > h) {
                                    rectangle.setSize(j - rectangle.y,
                                            rectangle.height);
                                    if (plusGrandRectangle == null) {
                                        plusGrandRectangle =
                                                new Rectangle(rectangle);
                                    } else if (getAire(plusGrandRectangle) < getAire(rectangle)) {
                                        plusGrandRectangle =
                                                new Rectangle(rectangle);
                                    }
                                    p = new Point((i - h) + 1, rectangle.y);
                                    continue;
                                }

                                // On ne modifie pas les rectangles ayant une
                                // hauteur inférieure ou égale au rectangle
                                // actuel
                                tmp.push(rectangle);
                            }

                            // Création d'un nouveau rectangle ayant la hauteur
                            // du rectangle actuelle et la position initiale du
                            // rectangle le précédent
                            tmp.push(new Rectangle(p, new Dimension(0, h)));
                            pile = tmp;
                        }
                    } else {
                        pile.push(new Rectangle(0, 0, 0, h));
                    }
                }

                // Calcul de toutes les aires des rectangles pour déterminer le
                // plus grand rectangle du dallage
                while (!pile.isEmpty()) {
                    final Rectangle rectangle = pile.pop();
                    rectangle.setSize(hauteurs[i].length - rectangle.y,
                            rectangle.height);
                    if (plusGrandRectangle == null) {
                        plusGrandRectangle = new Rectangle(rectangle);
                    } else if (getAire(plusGrandRectangle) < getAire(rectangle)) {
                        plusGrandRectangle = new Rectangle(rectangle);
                    }
                }
            }
        }
        return plusGrandRectangle == null ? null : new AlgoResultat(
                plusGrandRectangle);
    }
}
