package edu.ensimag.algo.partie26;

import java.io.PrintStream;

/**
 * Implémentation d'une pile reposant sur une liste d'objets simplement chainés.
 *
 * Ces objets sont encapsulés dans d'autres objets pour permettre la mise en
 * place du chainage.
 *
 * Cette classe exploite les capacités en termes de généricité de Java (pour
 * gagner du temps, elle a été réalisée avant la partie de TP qui l'utilise,
 * d'où le choix de la généricité).
 *
 */
public class Pile<T> {
    /**
     * Cette classe permet de stocker un objet tout en maintenant une référence
     * vers un autre objet dans une liste simplement chainée.
     *
     */
    protected class Boite<E> {
        protected Boite<E> autreBoite;
        protected E objetStocke;

        /**
         * Instanciation d'une boite, affectation de son contenu et mise en
         * mémoire d'une autre boite.
         * 
         * @param objetStocke
         *            L'objet à stocker, de type défini par E
         * @param autreBoite
         *            Une autre boite
         */
        public Boite(final E objetStocke, final Boite<E> autreBoite) {
            this.autreBoite = autreBoite;
            this.objetStocke = objetStocke;
        }

        /**
         * Retourne une référence vers la boite associée à cette instance de
         * boite.
         * 
         * @return L'autre boite
         */
        public Boite<E> getAutreBoite() {
            return this.autreBoite;
        }

        /**
         * Retourne une référence vers l'objet stocké dans la boite.
         * 
         * @return Objet stocké dans la boite.
         */
        public E getObjetStocke() {
            return this.objetStocke;
        }
    }

    protected Boite<T> dernierElementListe;

    /**
     * Création d'une pile vide.
     */
    public Pile() {
        this.dernierElementListe = null;
    }

    /**
     * Vide la pile
     */
    public void empty() {
        // Le garbage collector viendra désallouer les
        // ressources consommées automatiquement
        this.dernierElementListe = null;
    }

    /**
     * Indique si la pile contient un ou plusieurs éléments
     * 
     * @return True si la pile est pas vide
     */
    public boolean isEmpty() {
        return this.dernierElementListe == null;
    }

    /**
     * Retourne le premier élément de la pile.
     * 
     * @return Premier élément de la pile
     * @throws Exception
     *             si la pile est vide
     */
    public T pop() {
        if (!this.isEmpty()) {
            final T dernierObjetStocke =
                    this.dernierElementListe.getObjetStocke();

            // Le dernier élément de la pile devient maintenant l'avant-dernier
            this.dernierElementListe = this.dernierElementListe.getAutreBoite();

            return dernierObjetStocke;
        }

        throw new RuntimeException("Pile vide");
    }

    /**
     * Permet d'afficher l'état de la pile, l'élément le plus en haut de
     * l'affichage généré est le plus récemment ajouté.
     * 
     * @param p
     *            Flux de sortie pour l'affichage
     */
    public void printEtatPile(final PrintStream p) {
        Boite<T> boiteAMontrer = this.dernierElementListe;
        while (boiteAMontrer != null) {
            p.println(boiteAMontrer.getObjetStocke().toString());

            // Déplacement vers une boite un peu moins récemment ajoutée
            boiteAMontrer = boiteAMontrer.getAutreBoite();
        }
    }

    /**
     * Ajout d'un élément dans la pile
     * 
     * @param objet
     *            Element à ajouter à la pile
     */
    public void push(final T objet) {
        // Création d'une nouvelle boite contenant le nouvel objet et
        // gardant en mémoire une référence vers l'ancien dernier élément
        final Boite<T> nouvelleBoite =
                new Boite<T>(objet, this.dernierElementListe);

        this.dernierElementListe = nouvelleBoite;
    }
}
